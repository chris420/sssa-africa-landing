import { withTheme, type ThemeProps } from "@rjsf/core";
import { type BaseInputTemplateProps, getInputProps } from "@rjsf/utils";
import validator from "@rjsf/validator-ajv8";
import type { RJSFSchema } from "@rjsf/utils";
import { useState, ChangeEvent, FocusEvent } from "react";

function BaseInputTemplate(props: BaseInputTemplateProps) {
  const {
    schema,
    id,
    options,
    label,
    value,
    type,
    placeholder,
    required,
    disabled,
    readonly,
    autofocus,
    onChange,
    onChangeOverride,
    onBlur,
    onFocus,
    rawErrors,
    hideError,
    uiSchema,
    registry,
    formContext,
    hideLabel,
    ...rest
  } = props;
  const onTextChange = ({
    target: { value: val },
  }: ChangeEvent<HTMLInputElement>) => {
    // Use the options.emptyValue if it is specified and newVal is also an empty string
    onChange(val === "" ? options.emptyValue || "" : val);
  };
  const onTextBlur = ({
    target: { value: val },
  }: FocusEvent<HTMLInputElement>) => onBlur(id, val);
  const onTextFocus = ({
    target: { value: val },
  }: FocusEvent<HTMLInputElement>) => onFocus(id, val);

  const inputProps = { ...rest, ...getInputProps(schema, type, options) };
  const hasError = (rawErrors || []).length > 0 && !hideError;

  console.log("BaseInputTemplate, props", props);

  return (
    <>
      (before label)
      <label htmlFor={id}>{label}</label>
      {required ? "*" : ""}
      (before input)
      <input
        id={id}
        className="py-3 px-4 block w-full text-md rounded-lg border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900"
        value={value}
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readonly}
        required={required}
        autoFocus={autofocus}
        onChange={onChangeOverride || onTextChange}
        onBlur={onTextBlur}
        onFocus={onTextFocus}
        {...inputProps}
      />
      {hasError && <div>Errors: {rawErrors}</div>}
    </>
  );
}

const theme: ThemeProps = {
  widgets: {},
  templates: {
    BaseInputTemplate,
    ButtonTemplates: {
      // SubmitButton: () => <div>(submit)</div>,
    },
  },
};

const ThemedForm = withTheme(theme);

const schema: RJSFSchema = {
  $schema: "http://json-schema.org/schema#",
  type: "object",
  properties: {
    yachtName: {
      type: "string",
    },
    sailNumber: {
      type: "string",
    },
    ownerName: {
      type: "string",
    },
    ownerEmail: {
      type: "string",
    },
    skipperName: {
      type: "string",
    },
    skipperQualification: {
      type: "string",
    },
    commitmentFee: {
      type: "boolean",
    },
    confirmDataCollection: {
      type: "boolean",
    },
    category: {
      type: "string",
    },
    copilotName: {
      type: "null",
    },
    skipperTel: {
      type: "string",
    },
    acceptRisk: {
      type: "boolean",
    },
  },
  required: ["acceptRisk", "yachtName"],
};

const uiSchema = {
  yachtName: {
    "ui:autofocus": true,
    "ui:emptyValue": "",
    "ui:placeholder":
      "ui:emptyValue causes this field to always be valid despite being required",
    "ui:autocomplete": "family-name",
    "ui:enableMarkdownInDescription": true,
    "ui:description":
      "Make text **bold** or *italic*. Take a look at other options [here](https://markdown-to-jsx.quantizor.dev/).",
  },
  commitmentFee: {
    "ui:title": "Committment Fee",
    "ui:description": "This is to confirm...",
  },
};

const log = (type: string) => console.log.bind(console, type);

function FormView({
  id,
  children,
}: {
  id?: string;
  children: React.ReactNode;
}) {
  return (
    <section id={id}>
      <div className="relative mx-auto max-w-7xl px-4 md:px-6 py-12 md:py-16 lg:py-20 text-default">
        <div className="flex flex-col max-w-xl mx-auto rounded-lg backdrop-blur border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900 shadow p-4 sm:p-6 lg:p-8 w-full">
          {children}
        </div>
      </div>
    </section>
  );
}

export default function MyTestForm() {
  const [show, setShow] = useState(false);
  const [data, setData] = useState<any>({});
  return (
    <FormView>
      <button onClick={() => setShow(!show)}>{show ? "Hide" : "Show"}</button>
      {show && <div>some detail...</div>}
      <ThemedForm
        formData={data}
        schema={schema}
        uiSchema={uiSchema}
        validator={validator}
        onChange={(e) => setData(e.formData)}
        onSubmit={(e) => {
          console.log("onSubmit", e.formData);
        }}
        onError={log("errors")}
      />
      <pre
        contentEditable
        onInput={(e) =>
          setData(JSON.parse(e.currentTarget.textContent || "") || {})
        }
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(data, null, 2) || "",
        }}
      />
    </FormView>
  );
}
