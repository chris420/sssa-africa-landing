import { useEffect, useState, type HTMLInputTypeAttribute, type PropsWithChildren, type ReactElement, type ReactNode } from "react";
import { FormProvider, useForm, useFormContext, type DefaultValues, type FieldValues } from "react-hook-form";
import Headline from "./ui/Headline";
import Button from "./ui/Button";
import { IconChevronCompactRight, IconChevronRight } from "@tabler/icons-react";

type Test = {
  name?: string;
}

function FormView({ id, children }) {

  return (
    <section id={id}>
      <div className="relative mx-auto max-w-7xl px-4 md:px-6 py-12 md:py-16 lg:py-20 text-default">
        <div className="flex flex-col max-w-xl mx-auto rounded-lg backdrop-blur border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900 shadow p-4 sm:p-6 lg:p-8 w-full">
          {children}
        </div>
      </div>
    </section>
  );

}

function TextInput({ name, type = "text", label, helpText, required }: { name: string, type?: HTMLInputTypeAttribute, label: string, helpText?: ReactNode, required?: boolean }) {
  const { register } = useFormContext();
  return (
    <div className="mb-6">
      <label htmlFor={name} className="block text-sm font-medium">
        {label}
      </label>
      <input
        type={type}
        required={required}
        id={name}
        className="py-3 px-4 block w-full text-md rounded-lg border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900"
        {...register(name)}
      />
      { helpText && <p className="text-sm text-gray-500">{helpText}</p> }
    </div>
  );
}

function Form<T extends FieldValues>({ id, defaultValues, children, onSubmit }: {
  id: string,
  defaultValues: DefaultValues<T>,
  onSubmit: (data: T) => Promise<void>,
} & PropsWithChildren) {
  const methods = useForm<T>({ defaultValues });

  return <FormProvider {...methods}>
    <form onSubmit={methods.handleSubmit(async (data: T) => {
      console.log('data', data);
      try {
        await onSubmit(data);
      } catch (e) {
        console.error(e);
        methods.setError('root.onSubmit', { type: 'submit', message: e.message });
        throw e;
      }
    })}>
      <FormView id={id}>{children}</FormView>
    </form>
  </FormProvider>;
}

function NextSteps() {
  const { formState: { isSubmitSuccessful } } = useFormContext();

  return isSubmitSuccessful ? (
    <>
      <div className="mt-6 text-center">
        <p className="text-lg font-medium">Thank you for entering!</p>
        <p className="text-sm text-gray-500">We will contact you in due course with further details.</p>
      </div>
      <div className="mt-6 text-center">
        <a className="btn-primary" href="https://social.sssa.africa/public/local" target="_blank">Check What's New <IconChevronRight /></a>
      </div>
    </>
  ) : null;
}

function CheckboxInput({ name, label, required, helpText }: { name: string, label: string, required?: boolean, helpText?: ReactNode }) {
  const { register } = useFormContext();

  return <div className="mb-6">
    <div className="flex items-start">
      <div className="flex mt-0.5">
        <input
          id={name}
          type="checkbox"
          required={required}
          {...register(name)}
          className="cursor-pointer mt-1 py-3 px-4 block w-full text-md rounded-lg border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900"
        />
      </div>
      <div className="ml-3">
        <label htmlFor={name} className="cursor-pointer select-none text-sm text-gray-600 dark:text-gray-400">
          {label}
        </label>
      </div>
    </div>
    { helpText && <p className="text-sm text-gray-500">{helpText}</p> }
  </div>
}

function RadioInput({ name, label, options, required, helpText }: { name: string, label: string, options: { value: string, label: string }[], required?: boolean, helpText?: ReactNode }) {
  const { register } = useFormContext();
  return (
    <div className="mb-6">
      <label className="block text-sm font-medium mb-2">
        {label}
      </label>
      {options.map(option => (
        <div key={option.value} className="flex items-center mb-2">
          <input
            type="radio"
            required={required}
            id={option.value}
            value={option.value}
            className="mr-2"
            {...register(name)}
          />
          <label htmlFor={option.value} className="text-md">
            {option.label}
          </label>
        </div>
      ))}
      { helpText && <p className="text-sm text-gray-500">{helpText}</p> }
    </div>
  );
}

function CopilotInput() {
  const { watch } = useFormContext();
  const [show, setShow] = useState(false);
  const category = watch('category');

  useEffect(() => {
    setShow(category === 'doublehanded');
  }, [category]);

  return show ? (
    <TextInput name="copilotName" required label="Co-pilot's Full Name" />
  ) : null;
}

type FormProps = {
  yachtName: string;
  sailNumber: string;
  ownerName: string;
  ownerEmail: string;
  skipperName: string;
  skipperQualification: string;
  commitmentFee: boolean;
  confirmDataCollection: boolean;
}

export default function RegistrationForm({ id }) {
  const [test, setTest] = useState<Test>({});

  return (
    <>
      <Headline subtitle="Please fill out the form below to enter the 2024 Canyon Cup. We will then contact you in due course with further details." />

      <Form id={id} defaultValues={{} as FormProps} onSubmit={async (data) => {
        console.log('submitted', data);
        // await new Promise(resolve => setTimeout(resolve, 1000));
        const json = JSON.stringify(data);

        console.log("json", json);

        const response = await fetch("https://sobbitrdyvpreevrxrgj.supabase.co/rest/v1/canyon_cup_2024_registrations?apikey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNvYmJpdHJkeXZwcmVldnJ4cmdqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTU3OTY1NjIsImV4cCI6MjAzMTM3MjU2Mn0.LnHpq8o1X1fpRfrygQJO-lEo2BuQnsjkMLgTqVrQbQ4", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: json,
        });
        if (response.status !== 200 && response.status !== 201) {
          console.log("response", response);
          let responseJson;
          try {
            responseJson = await response.json();
          } catch (e) {
            throw new Error(`Request failed, status ${response.status}. Please try again, or contact us.`);
          }
          console.log("responseJson", responseJson);
          throw new Error(`Request failed, status ${response.status}. ${responseJson.message}`);
        }
      }}>

        <TextInput required name="yachtName" label="Yacht Name" helpText="Specify the registered name of the yacht participating."/>
        <TextInput required name="sailNumber" label="Sail Number" />
        <TextInput required name="ownerName" label="Owner's Full Name" />
        <TextInput required type="email" name="ownerEmail" label="Owner's Email Address" />
        <TextInput required name="skipperName" label="Skipper's Full Name" />
        <TextInput required name="skipperTel" type="tel" label="Skipper's Mobile Number" />
        <RadioInput required name="skipperQualification" label="Skipper's Qualification" options={[
          { value: 'coastal', label: 'SA Sailing Coastal Skipper' },
          { value: 'offshore', label: 'SA Sailing Yachtmaster Offshore' },
          { value: 'ocean', label: 'SA Sailing Yachtmaster Ocean' },
        ]} helpText={<>To participate in the Canyon Cup, the skipper needs to hold one of these SA Sailing qualifications. See <a className="underline" href="https://www.sailing.org.za/what-we-do/skippertickets">SA Sailing's website</a> for details.</>} />
        <CheckboxInput required name="commitmentFee" label="I confirm payment of the commitment fee." helpText={
          <>You are kindly requested to pay a R500 commitment fee to the Little Optimist
            Trust. <a className="underline" href="https://thelittleoptimisttrust.org/donate/">Click here</a> for
            details. Please use the EFT option, and use "CC" and your boat name as reference.</>
        }/>
        <RadioInput required name="category" label="Category" options={[
          { value: 'singlehanded', label: 'Single Handed [ORC Club or NS Certificate with correct declared weight only.]' },
          { value: 'doublehanded', label: 'ORC Double Handed National Championship [ORC DH Certificate with correct declared weight only.]' },
          { value: 'three-up', label: 'Three Up [ORC Club or NS Certificate with correct declared weight only.]' },
        ]} />
        {/* <CopilotInput /> */}
        <CheckboxInput required name="acceptRisk" label="I confirm that the responsibility for a boat’s decision to participate in a race or to continue racing is hers 
alone." helpText={<>
  By participating in this event each competitor agrees and acknowledges that sailing is a potentially 
dangerous activity with inherent risks. These risks include strong winds and rough seas, sudden changes in 
weather, failure of equipment, boat handling errors, poor seamanship by other boats, loss of balance on an 
unstable platform and fatigue resulting in increased risk of injury. Inherent in the sport of sailing is the risk of 
permanent, catastrophic injury or death by drowning, trauma, hypothermia, or other causes. Competitors 
participate in the event entirely at their own risk, see RRS 3 - Decision to Race. The organizing authority, 
ORC South Africa, SA Sailing, SSSA, the race committee and race officer, the protest committee, the technical 
committee, the volunteers, and any other party involved in the organisation of the event will not accept any 
liability for material damage or personal injury or death sustained in conjunction with or prior to, during, or 
after the event. Each boat owner or his representative accepts these terms by submitting this entry form.
  </>} />
        <CheckboxInput required name="confirmDataCollection" label="I confirm that my data will be collected." helpText={
          <>By submitting this form, you agree to the collection and processing of your personal data as described in our <a className="underline" href="/privacy">Privacy Policy</a>.</>
        }/>

        <Button variant="primary" type="submit" text="Enter the Canyon Cup 2024" submitSuccessfulText=""/>

        <NextSteps />

      </Form>
    </>
  );
}
