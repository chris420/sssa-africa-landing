import { useFormContext } from 'react-hook-form';
import { IconLoader, IconLoader2 } from "@tabler/icons-react";

export default function Button({ text, target, icon, variant, submitSuccessfulText = 'Done' }: { text: string, target?: string, icon?: string, variant: 'primary' | 'secondary' | 'tertiary' | 'link', type: 'button' | 'submit', submitSuccessfulText?: string }) {
  const { formState: { isDirty, isValid, isSubmitting, isSubmitSuccessful, isSubmitted, errors } } = useFormContext();

  const disabled = isSubmitting || isSubmitSuccessful;
  let btnClassName = variant === 'primary' ? 'btn-primary' : 'btn-secondary';
  if (isSubmitSuccessful) {
    btnClassName = '';
  }

  return variant === 'link' ? (
    <a target={target} className={`
        py-3 px-4 text-md rounded-lg
      `}
    >
      {text}
    </a>
  ) : (
    <>
      { errors && errors.root && errors.root.onSubmit && <div className="text-red-500">{errors.root.onSubmit.message}</div> }
      <button disabled={disabled} className={`
          py-3 px-4 text-md rounded-lg
          ${disabled ? 'btn-disabled' : 'btn-enabled'}
          ${disabled ? 'cursor-not-allowed' : 'cursor-pointer'}
          ${btnClassName}
        `}
      >
        {isSubmitting
          ? <><span className="animate-spin mr-3"><IconLoader2 /></span>Submitting...</>
          : isSubmitSuccessful ? submitSuccessfulText : text
        }
      </button>
    </>
  );
}

