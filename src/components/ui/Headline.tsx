import type { Headline as Props } from "~/types";
import { twMerge } from "tailwind-merge";

export default function Headline({
  title = "",
  subtitle = "",
  tagline,
  classes = {},
}: Props) {
  const {
    container: containerClass = "max-w-3xl",
    title: titleClass = "text-3xl md:text-4xl ",
    subtitle: subtitleClass = "text-xl",
  } = classes;

  return (
    <div
      className={twMerge("mb-8 md:mx-auto md:mb-8 text-center", containerClass)}
    >
      {tagline && (
        <p className="text-base text-secondary dark:text-blue-200 font-bold tracking-wide uppercase">{tagline}</p>
      )}
      {title && (
        <h2
          className={twMerge(
            "font-bold leading-tighter tracking-tighter font-heading text-heading text-3xl",
            titleClass
          )}
        >{title}</h2>
      )}

      {subtitle && (
        <p
          className={twMerge("mt-4 text-muted", subtitleClass)}
        >{subtitle}</p>
      )}
    </div>
  );
}

